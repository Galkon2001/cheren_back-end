const ACCESS_LEVEL = {
  USER: 1,
  ADMIN: 2
};

module.exports = {
  ACCESS_LEVEL
};
