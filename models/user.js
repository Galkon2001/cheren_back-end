const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const { ACCESS_LEVEL } = require("../constant");

const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    email: {
      type: String,
      unique: [true, "must be unique"],
      required: "can't be blank"
      //match: [emailRegexp, "is invalid"]
      //   validate: [
      //     {
      //       validator(value) {
      //         return emailRegexp.test(value);
      //       },
      //       msg: "email is invalid"
      //     }
      //   ]
    },
    password: { type: String, required: true },
    username: { type: String, required: true, match: [/^[a-zA-Z0-9!+\-#`.'_]+$/, "username is invalid"] },
    name: String,
    token: String,
    accessLevel: { type: Number, default: ACCESS_LEVEL.USER },
    isActive: { type: Boolean, default: true },
    isDeleted: { type: Boolean, default: false }
  },
  { versionKey: false, timestamps: true }
);

userSchema.methods.comparePassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

userSchema.statics.findFor = function(query, projection, options, next) {
  return this.find({ isDeleted: false, ...query }, projection, options, next);
};
userSchema.statics.findOneFor = function(query, projection, options, next) {
  return this.findOne({ isDeleted: false, ...query }, projection, options, next);
};
userSchema.statics.findOneForAndUpdate = function(query, update, options, next) {
  return this.findOneAndUpdate({ isDeleted: false, ...query }, update, options, next);
};
userSchema.statics.countDocumentsFor = function(filter, callback) {
  return this.countDocuments({ isDeleted: false, ...filter }, callback);
};

const Users = mongoose.model("User", userSchema);

module.exports = Users;
