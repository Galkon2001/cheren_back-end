const dotenv = require("dotenv");
dotenv.config();
const host = "localhost:27017";

module.exports = {
  PORT: 5000,
  mongoose: {
    URL: `mongodb://${host}/wf` //${process.env.TEST_ENV === 'true' ? '-test' : ''}
  },
  BODY_PARSER_LIMIT: "50mb",
  AUTH_HEADER_NAME: "authorization",
  PUBLIC_FOLDER: "public",
  APP_DIR: "build"
};
