const express = require("express");
const mongoose = require("mongoose");
const chalk = require("chalk");
const passport = require("passport");
const cors = require("cors");
const bodyParser = require("body-parser");

const Config = require("./config");
//const cookieParser = require("cookieParser");
const routes = require("./routes");
const port = process.env.PORT || 5000;

mongoose.connect("mongodb://localhost/wf", { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
const db = mongoose.connection;
db.on("error", err => console.log(chalk.red("connection to data base failed:"), err));
db.once("open", function() {
  console.log(chalk.blue("conected to data base."));
  initApp();
});

const initApp = () => {
  const app = express();
  app.use(
    bodyParser.urlencoded({
      limit: Config.BODY_PARSER_LIMIT,
      extended: false
    })
  );
  app.use(
    bodyParser.json({
      limit: Config.BODY_PARSER_LIMIT
    })
  );
  //app.use(express.static(path.join(__dirname, `../${Config.APP_DIR}`)));
  //app.use(cookieParser())
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(cors());
  require("./middleware/passport")(passport);

  app.use("/api/auth", routes.auth);

  app.get("/", (req, res) => {
    res.status(200).json("working");
  });

  app.listen(port, () =>
    console.log(chalk.blue(`server has been started on port ` + chalk.rgb(228, 240, 51).bold(`${port}`)))
  );
};
