const passport = require("passport");
const Config = require("../config");

module.exports = (req, res, next) => {
  const token = req.headers[Config.AUTH_HEADER_NAME];
  if (!token) {
    return res.status(401).json({
      status: false,
      message: "Not authenticated"
    });
  }

  passport.authenticate("jwt", { session: false }, (err, user, info) => {
    if (err) {
      return res.status(401).json({
        status: false,
        message: err
      });
    } else if (!user) {
      return res.status(404).json({
        status: false,
        message: info ? info.message : "no user"
      });
    }
    req.user = user;

    if (!req.isAuthenticated()) {
      return res.status(401).json({
        status: false,
        message: "Not authenticated"
      });
    }

    if (user.token !== token) {
      return res.status(401).json({
        status: false,
        message: "Expired token"
      });
    }

    next();
  })(req, res, next);
};
