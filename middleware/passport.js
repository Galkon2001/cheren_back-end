const LocalStrategy = require("passport-local").Strategy;
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

const User = require("../models/user");

module.exports = passport => {
  passport.serializeUser((user, done) => {
    console.log("serializeUser", user._id);
    done(null, user._id);
  });

  passport.deserializeUser((id, done) => {
    console.log("deserializeUser", id);
    User.findOneFor({ _id: id }, (err, user) => {
      if (err || !user) {
        return done(err);
      }
      done(null, user, {
        status: true,
        message: "Authenticate success."
      });
    });
  });

  passport.use(
    new LocalStrategy(
      {
        usernameField: "email",
        passwordField: "password"
      },
      (email, password, done) => {
        console.log("LocalStrategy", email, password);
        User.findOne({ email }, (err, user) => {
          if (err || !user) {
            return done(err, false, {
              status: false,
              message: (err && err.message) || "User not found"
            });
          }
          if (!user.isActive) {
            return done(err, false, {
              message: "User isn't acitve"
            });
          }
          if (!user.comparePassword(password)) {
            return done(null, false, {
              status: false,
              message: "Wrong password"
            });
          }
          user.set({
            token: `Bearer ${jwt.sign(
              {
                _id: user._id,
                email: user.email
              },
              secret
            )}`
          });
          user.save(err => {
            done(err, user);
          });
        });
      }
    )
  );
  passport.use(
    new JwtStrategy(
      {
        secretOrKey: secret,
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
      },
      (payload, done) => {
        User.findOneFor({ _id: payload._id }, (err, user) => {
          if (err) {
            return done(err, false);
          }
          if (!user) {
            console.log("JwtStrategy: no user found");
            return done(null, false);
          }
          done(null, user, {
            status: true,
            message: "Authenticate success."
          });
        });
      }
    )
  );
};
