const express = require("express");
const router = express.Router({ prefix: "/users" });
const bcrypt = require("bcryptjs");
const User = require("../models/user");
const Yup = require("yup");
const passport = require("passport");
const auth = require("../middleware/auth");

const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const RegisterSchema = Yup.object().shape({
  password: Yup.string()
    .min(8, "Password should have at least 8 symbols!")
    .matches(
      /(?=..*\d)(?=.*[a-zA-Z])/, //(?=..*[-+_!@#$%^&*.,?])
      "Password should have at least 1 digit and 1 letter" //1 symbol
    )
    .required("Password required"),
  email: Yup.string().matches(emailRegexp)
});

const hashPassword = password =>
  new Promise((resolve, reject) =>
    bcrypt.genSalt(10, function(err, salt) {
      if (err) {
        console.log(genSaltErr, err);
        reject();
      }
      bcrypt.hash(password, salt, function(err, hash) {
        if (err) {
          console.log(genHashErr, err);
          reject();
        }
        resolve(hash);
      });
    })
  );
router.use((req, res, next) => {
  const { email } = req.body;
  if (email) req.body.email = email.toLowerCase();
  next();
});
router.post("/signin", (req, res, next) => {
  console.log("signin", req.body);
  passport.authenticate("local", (err, user, msg) => {
    console.log("authenticate");
    if (err) {
      return next(err);
    } else if (!user) {
      if (msg) {
        return res.status(404).json({
          status: false,
          message: msg.message,
          data: err
        });
      }
      return res.status(404).json({
        status: false,
        message: "User not found",
        data: err
      });
    }
    req.login(user, err => {
      console.log("login");
      if (err) {
        return next(err);
      }
      res.json({
        status: true,
        message: "User found",
        data: user
      });
    });
  })(req, res, next);
});
// router.post("/signin", async (req, res, next) => {
//   //const { email, password } = { email: process.env.DEFAULT_EMAIL, password: process.env.DEFAULT_PASSWORD };
//   const { email, password } = req.body;
//   console.log("/signin", req.body);
//   const isValid = await RegisterSchema.validate({
//     email,
//     password
//   }).catch(err => {
//     console.log("router.postError", err);
//     res.status(400).json(err.message);
//   });
//   if (!isValid) return;
//   passport.authenticate("local", (err, user, msg) => {
//     console.log("authenticate  ============>>>>>>");
//     if (err) {
//       return next(err);
//     } else if (!user) {
//       if (msg) {
//         return res.status(404).json({
//           status: false,
//           message: msg.message,
//           data: err
//         });
//       }
//       return res.status(404).json({
//         status: false,
//         message: "User not found",
//         data: err
//       });
//     }
//     console.log(req.login.toString());
//     req.login(user, err => {
//       console.log("login");
//       if (err) {
//         console.log("err1", err);
//         return next(err);
//       }
//       //res.status(200).json(`loged in: welcome ${username}`);
//       return res.json({
//         status: true,
//         message: "User found",
//         data: user
//       });
//     });
//   })(req, res, next);
//   //passport.authenticate("local", { session: false });
// });

router.get("/signout", (req, res) => {
  res.status(200).json("signout");
});

router.get("/testinfo", auth, (req, res) => {
  res.status(200).json("testinfo");
});

router.get("/register", async (req, res) => {
  const { password, email, username = "anonimus" } = {
    email: process.env.DEFAULT_EMAIL,
    passwoxrd: process.env.DEFAULT_PASSWORD
  };
  let isValidError;
  await RegisterSchema.validate({
    email,
    password
  }).catch(err => (isValidError = err));
  if (isValidError) {
    console.log(isValidError);
    res.status(400).json(isValidError.message);
    return;
  }
  const hash = await hashPassword(password);
  if (hash) {
    try {
      await User.create({
        email,
        password: hash,
        username
      });
      res.status(200).json(`registered: welcome ${username} ${hash}`);
    } catch (err) {
      console.log(err);
      res.status(400).json(err.message);
    }
  }
});

module.exports = router;
